package com.example.mapper

import com.example.data.mapper.ProjectMapper
import com.example.data.model.ProjectEntity
import com.example.domain.model.Project
import com.example.factory.ProjectFactory
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import kotlin.test.assertEquals


@RunWith(JUnit4::class)
class ProjectMapperTest {

    private val mapper = ProjectMapper()

    @Test
    fun mapFromEntityMapsData(){
        val entity = ProjectFactory.makeProjectEntity()
        val model  = mapper.mapFromEntity(entity)
         assertEqualData(entity,model)
    }


    @Test
    fun mapToEntityMapsData(){
        val model = ProjectFactory.makeProject()
        val entity  = mapper.mapToEntity(model)
        assertEqualData(entity,model)
    }


    private fun assertEqualData(entity: ProjectEntity, model: Project) {
        assertEquals(entity.id,model.id)
        assertEquals(entity.name,model.name)
        assertEquals(entity.fullName,model.fullName)
        assertEquals(entity.starCount,model.starCount)
        assertEquals(entity.dateCreated,model.dateCreated)
        assertEquals(entity.ownerName,model.ownerName)
        assertEquals(entity.ownerAvatar,model.ownerAvatar)
        assertEquals(entity.isBoomarked,model.isBookMarked)
    }


}