package com.example.factory

import com.example.data.model.ProjectEntity
import com.example.domain.model.Project
import com.example.factory.DataFactory.randomBoolean
import com.example.factory.DataFactory.randomUUID


object ProjectFactory {

    fun  makeProjectEntity():ProjectEntity{
        return ProjectEntity(randomUUID(),randomUUID(),randomUUID(),randomUUID(),randomUUID(),randomUUID(),randomUUID(),randomBoolean())
    }


    fun makeProject(): Project {
        return Project(randomUUID(),randomUUID(),randomUUID(),randomUUID(),randomUUID(),randomUUID(),randomUUID(),randomBoolean())
    }



}