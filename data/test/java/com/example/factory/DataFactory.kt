package com.example.factory

import com.example.domain.model.Project
import java.util.*
import java.util.concurrent.ThreadLocalRandom

object DataFactory {


    fun randomUUID():String{
        return UUID.randomUUID().toString()
    }

    fun randomBoolean():Boolean{
        return Math.random() < 0.5
    }



    fun randomString(): String {
        return UUID.randomUUID().toString()
    }

    fun randomInt(): Int {
        return ThreadLocalRandom.current().nextInt(0, 1000 + 1)
    }

    fun randomLong(): Long {
        return randomInt().toLong()
    }


    fun makeProject():Project{
        return Project(
            randomUUID(),
            randomUUID(),
            randomUUID(),
            randomUUID(),
            randomUUID(),
            randomUUID(),
            randomUUID(),
            randomBoolean()
        )
    }

    fun makeProjectList(count:Int):List<Project>{
        val projects = mutableListOf<Project>()
        repeat(count){
            projects.add(makeProject())
        }

        return projects
    }
}