package com.example.store

import com.example.data.ProjectDataRepository
import com.example.data.mapper.ProjectMapper
import com.example.data.model.ProjectEntity
import com.example.data.repository.ProjectDataStore
import com.example.data.repository.ProjectsCache
import com.example.data.store.ProjectDataStoreFactory
import com.example.domain.model.Project
import com.example.factory.DataFactory
import com.example.factory.ProjectFactory
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class ProjectRepositoryTest {

    private val mapper = mock<ProjectMapper>()
    private val factory = mock<ProjectDataStoreFactory>()
    private val store = mock<ProjectDataStore>()
    private val cache = mock<ProjectsCache>()
    private val repository = ProjectDataRepository(mapper, cache, factory)


    @Before
    fun setUp() {
        stubFactoryGetDataStore()
        stubFactoryGetCachedDataStore()
        stubIsCacheExpired(Single.just(false))
        stubAreProjectCached(Single.just(false))
        stubSaveProject(Completable.complete())
    }

    @Test
     fun getProjectsCompetes()
    {
        stubGetProjects(Observable.just(listOf(ProjectFactory.makeProjectEntity())))
        stubMapper(ProjectFactory.makeProject(), any())
        val testObserver = repository.getProjects().test()
        testObserver.assertComplete()
    }

    @Test
    fun getBookmarkedProjectsCompetes()
    {
        stubGetBookmarkedProjects(Observable.just(listOf(ProjectFactory.makeProjectEntity())))
        stubMapper(ProjectFactory.makeProject(), any())
        val testObserver = repository.getBookmarkedProjects().test()
        testObserver.assertComplete()
    }



    @Test
    fun getsProjectReturnsData(){
        val projectEntity = ProjectFactory.makeProjectEntity()
         val project = ProjectFactory.makeProject()
        stubGetProjects(Observable.just(listOf(projectEntity)))
        stubMapper(project,projectEntity)

        val testObserver = repository.getProjects().test()
        testObserver.assertValue(listOf(project))
    }


    @Test
    fun getBookmarkedProjectReturnsData(){
        val projectEntity = ProjectFactory.makeProjectEntity()
        val project = ProjectFactory.makeProject()
        stubGetBookmarkedProjects(Observable.just(listOf(projectEntity)))
        stubMapper(project,projectEntity)

        val testObserver = repository.getBookmarkedProjects().test()
        testObserver.assertValue(listOf(project))
    }


    @Test
    fun bookmarkProjectCompletes(){
        stubBookmarkProject(Completable.complete())
        val testObserver = repository.bookmarkProject(DataFactory.randomString()).test()
        testObserver.assertComplete()
    }

    private fun stubBookmarkProject(completable: Completable){
        whenever(store.setProjectAsBookmarked(any())).thenReturn(completable)
    }


    @Test
    fun unbookmarkProjectCompletes(){
        unbookmarkProject(Completable.complete())
        val testObserver = repository.unbookmarkProject(DataFactory.randomString()).test()
        testObserver.assertComplete()
    }

    private fun unbookmarkProject(completable: Completable){
        whenever(store.setProjectAsNotBookmarked(any())).thenReturn(completable)
    }

    private  fun stubIsCacheExpired(single:Single<Boolean>){
        whenever(cache.isProjectCachedExpired()).thenReturn(single)

    }


    private  fun stubAreProjectCached(single:Single<Boolean>){
        whenever(cache.areProjectsCached()).thenReturn(single)

    }


    private fun stubMapper(model: Project, entity: ProjectEntity){
        whenever(mapper.mapFromEntity(entity)).thenReturn(model)
    }

    private fun stubGetProjects(observable: Observable<List<ProjectEntity>>){
        whenever(store.getProjects())
            .thenReturn(observable)
    }
    private fun stubGetBookmarkedProjects(observable: Observable<List<ProjectEntity>>){
        whenever(store.getBookmarkedProjects())
            .thenReturn(observable)
    }

    private fun stubFactoryGetDataStore() {
        whenever(factory.getDataStore(any(),any()))
            .thenReturn(store)
    }



    private fun stubFactoryGetCachedDataStore() {
        whenever(factory.getCachedDataStore())
            .thenReturn(store)
    }

    private fun stubSaveProject(completable: Completable){
        whenever(store.saveProjects(any())).thenReturn(completable)
    }





}