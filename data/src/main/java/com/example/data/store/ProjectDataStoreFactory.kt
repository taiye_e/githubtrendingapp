package com.example.data.store

import com.example.data.repository.ProjectDataStore
import javax.inject.Inject

class ProjectDataStoreFactory @Inject constructor(private val projectsCacheDataStore: ProjectsCacheDataStore,
                                                  private val projectRemoteDataStore: ProjectRemoteDataStore) {

          open fun getDataStore(projectsCached:Boolean, cacheExpired:Boolean):ProjectDataStore{

              return if(projectsCached && !cacheExpired){
                  projectsCacheDataStore
              }else{
                  projectRemoteDataStore
              }

          }


          open fun getCachedDataStore():ProjectDataStore{
              return projectsCacheDataStore
          }
}