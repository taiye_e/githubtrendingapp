package com.example.data.store

import com.example.data.model.ProjectEntity
import com.example.data.repository.ProjectDataStore
import com.example.data.repository.ProjectsRemote
import io.reactivex.Completable
import io.reactivex.Observable
import java.lang.UnsupportedOperationException
import javax.inject.Inject

class ProjectRemoteDataStore @Inject constructor(private val projectsRemote: ProjectsRemote):ProjectDataStore {
    override fun getProjects(): Observable<List<ProjectEntity>> {
      return  projectsRemote.getProjects()
    }

    override fun getBookmarkedProjects(): Observable<List<ProjectEntity>> {
        throw  UnsupportedOperationException("Getting Bookmarked project is not supported")
    }

    override fun setProjectAsBookmarked(projectId: String): Completable {
        throw  UnsupportedOperationException("Setting  project as bookmarked is not supported")

    }

    override fun setProjectAsNotBookmarked(projectId: String): Completable {
        throw  UnsupportedOperationException("Setting project as bookmarked  is not supported")
    }


    override fun clearProjects(): Completable {
        throw  UnsupportedOperationException("Clearing projects is not supported")
    }

    override fun saveProjects(projects: List<ProjectEntity>): Completable {
        throw  UnsupportedOperationException("Saving   project is not supported here..")
    }
}