package com.example.data;

import com.example.data.mapper.ProjectMapper
import com.example.data.repository.ProjectsCache
import com.example.data.store.ProjectDataStoreFactory
import com.example.domain.model.Project
import com.example.domain.repository.ProjectRepository
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.functions.BiFunction
import javax.inject.Inject;

public class ProjectDataRepository @Inject constructor(
    private val mapper: ProjectMapper,
    private val cache: ProjectsCache,
    private val factory: ProjectDataStoreFactory
) : ProjectRepository {

    override fun getProjects(): Observable<List<Project>> {
        return Observable.zip(cache.areProjectsCached().toObservable(),
            cache.isProjectCachedExpired().toObservable(),
            BiFunction<Boolean, Boolean, Pair<Boolean, Boolean>> { areCached, isExpired ->
                Pair(areCached, isExpired)
            })
            .flatMap {
                 factory.getDataStore(it.first, it.second).getProjects()
             }
            .flatMap { projects ->
                factory.getCachedDataStore()
                       .saveProjects(projects)
                       .andThen(Observable.just(projects))
            }.map {
                it.map {
                    mapper.mapFromEntity(it)
                }
            }
        }

    override fun bookmarkProject(projectId: String): Completable {
      return  factory.getCachedDataStore().setProjectAsBookmarked(projectId)
    }

    override fun unbookmarkProject(projectId: String): Completable {
        return factory.getCachedDataStore().setProjectAsNotBookmarked(projectId)
    }

    override fun getBookmarkedProjects(): Observable<List<Project>> {
        return factory.getCachedDataStore().getBookmarkedProjects()
            .map {
                it.map { mapper.mapFromEntity(it)
                }
            }
    }



}
