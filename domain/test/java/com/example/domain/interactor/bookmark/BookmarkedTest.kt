package com.example.domain.interactor.bookmark

import com.example.domain.executor.PostExecutionThread
import com.example.domain.interactor.bookmarked.GetBookmarkedProjects
import com.example.domain.model.Project
import com.example.domain.repository.ProjectRepository
import com.example.domain.test.ProjectFactory
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Observable
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations

class BookmarkedTest {

    private  lateinit var  getBookmarkedProjects: GetBookmarkedProjects
    @Mock lateinit var projectRepository: ProjectRepository
    @Mock lateinit var postExecutionThread: PostExecutionThread


    @Before
    fun setUp(){
        MockitoAnnotations.initMocks(this)
        getBookmarkedProjects = GetBookmarkedProjects(projectRepository, postExecutionThread)

    }

    @Test
    fun getBookmarkedProjectCompletes(){
        stubGetProjects(Observable.just(ProjectFactory.makeProjectList(2)))
        val testObservers = getBookmarkedProjects.buildCaseObservable().test()
        testObservers.assertComplete()
    }

    @Test
    fun getBookmarkedProjectReturnsData(){
        val projectsBookmarked = ProjectFactory.makeProjectList(2)
        stubGetProjects(Observable.just(projectsBookmarked))
        val testObservers = getBookmarkedProjects.buildCaseObservable().test()
        testObservers.assertValue(projectsBookmarked)
    }



    private  fun stubGetProjects(observable: Observable<List<Project>>){
        whenever(projectRepository.getBookmarkedProjects()).thenReturn(observable)

    }

}