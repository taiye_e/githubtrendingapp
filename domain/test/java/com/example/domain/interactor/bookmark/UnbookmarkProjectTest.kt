package com.example.domain.interactor.bookmark

import com.example.domain.executor.PostExecutionThread
import com.example.domain.interactor.bookmarked.UnbookmarkProject
import com.example.domain.repository.ProjectRepository
import com.example.domain.test.ProjectFactory
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Completable
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations

class UnbookmarkProjectTest {
    private  lateinit var  unbookmarkProject: UnbookmarkProject
    @Mock lateinit var projectRepository: ProjectRepository
    @Mock lateinit var postExecutionThread: PostExecutionThread


    @Before
    fun setUp(){
        MockitoAnnotations.initMocks(this)
        unbookmarkProject = UnbookmarkProject(projectRepository, postExecutionThread)
    }


    @Test
    fun unbookmarkProjectTest(){
        stubUnbookmarkProject(Completable.complete())
        val testObserver = unbookmarkProject.buildCaseCompeletable(UnbookmarkProject.Params.forProject(ProjectFactory.randomUUID())).test()
        testObserver.assertComplete()
    }


    fun stubUnbookmarkProject(completable: Completable){
       whenever(projectRepository.unbookmarkProject(any())).thenReturn(completable)
    }


}