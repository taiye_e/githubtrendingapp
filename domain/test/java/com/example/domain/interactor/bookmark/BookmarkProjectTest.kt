package com.example.domain.interactor.bookmark

import com.example.domain.executor.PostExecutionThread
import com.example.domain.interactor.bookmarked.BookmarkProject
import com.example.domain.repository.ProjectRepository
import com.example.domain.test.ProjectFactory
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Completable
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations

class BookmarkProjectTest {

    private  lateinit var  bookmarkedProject: BookmarkProject
    @Mock lateinit var projectRepository: ProjectRepository
    @Mock lateinit var postExecutionThread: PostExecutionThread


    @Before
    fun setUp(){
        MockitoAnnotations.initMocks(this)
        bookmarkedProject = BookmarkProject(projectRepository, postExecutionThread)

    }


    @Test
    fun bookmarkProjectComplete(){
        stubBookmarkProject(Completable.complete())
        val testObserver = bookmarkedProject.buildCaseCompeletable(BookmarkProject.Params.forProject(ProjectFactory.randomUUID())).test()
        testObserver.assertComplete()

    }

    @Test(expected = IllegalArgumentException::class)
    fun bookmarkProjectThrowsException(){
        bookmarkedProject.buildCaseCompeletable().test()
    }

    private  fun stubBookmarkProject(completable:Completable){
        whenever(projectRepository.bookmarkProject(any()))
            .thenReturn(completable)
    }
}