package com.example.domain.interactor.browse

import com.example.domain.executor.PostExecutionThread
import com.example.domain.interactor.browse.GetProjects
import com.example.domain.model.Project
import com.example.domain.repository.ProjectRepository
import com.example.domain.test.ProjectFactory
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Observable
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations

class GetProjectsTest {

    private  lateinit var  getProjects: GetProjects
    @Mock lateinit var projectRepository: ProjectRepository
    @Mock lateinit var postExecutionThread: PostExecutionThread


    @Before
    fun setUp(){
        MockitoAnnotations.initMocks(this)
        getProjects = GetProjects(projectRepository, postExecutionThread)

    }


    @Test
    fun getProjectsCompletes(){
        stubGetProjects(Observable.just(ProjectFactory.makeProjectList(2)))
        val testObserver = getProjects.buildCaseObservable().test()
        testObserver.assertComplete()
    }

    @Test
    fun getProjectsReturnsData(){
         val projects =  ProjectFactory.makeProjectList(2)
         stubGetProjects(Observable.just(projects))
         val testObserver = getProjects.buildCaseObservable().test()
         testObserver.assertValue(projects)
    }

    private fun stubGetProjects(observable: Observable<List<Project>>) {
        whenever(projectRepository.getProjects())
                .thenReturn(observable)
    }

}