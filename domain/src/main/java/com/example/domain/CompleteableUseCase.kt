package com.example.domain

import com.example.domain.executor.PostExecutionThread
import io.reactivex.Completable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.observers.DisposableCompletableObserver
import io.reactivex.schedulers.Schedulers

abstract  class CompleteableUseCase< in Params> constructor(
    private val postExecutionThread: PostExecutionThread
)
{
    private val disposables = CompositeDisposable()


      abstract  fun buildCaseCompeletable(params: Params?= null): Completable


    open fun execute(observer: DisposableCompletableObserver, params: Params?= null){
        val completable = this.buildCaseCompeletable(params)
            .subscribeOn(Schedulers.io())
            .observeOn(postExecutionThread.scheduler)
           addDisposable(completable.subscribeWith(observer))
    }



    fun dispose(){
        disposables.dispose()
    }


     fun addDisposable(disposable: Disposable){
        disposables.add(disposable)
    }

}