package com.example.domain.interactor.bookmarked

import com.example.domain.executor.PostExecutionThread
import com.example.domain.ObservableUseCase
import com.example.domain.model.Project
import com.example.domain.repository.ProjectRepository
import io.reactivex.Observable
import javax.inject.Inject

class GetBookmarkedProjects @Inject constructor(private  val projectRepository:ProjectRepository,postExecutionThread: PostExecutionThread)
                                            :
    ObservableUseCase<List<Project>, Nothing?>(postExecutionThread) {
    override fun buildCaseObservable(params: Nothing?): Observable<List<Project>> {
        return  projectRepository.getBookmarkedProjects()
    }
}