package com.example.domain.interactor.bookmarked

import com.example.domain.executor.PostExecutionThread
import com.example.domain.CompleteableUseCase
import com.example.domain.repository.ProjectRepository
import io.reactivex.Completable
import javax.inject.Inject

open class UnbookmarkProject @Inject constructor(private val projectRepository: ProjectRepository,postExecutionThread: PostExecutionThread):
    CompleteableUseCase<UnbookmarkProject.Params>(postExecutionThread) {


    data class Params constructor(val projectId:String){
        companion object{
            fun forProject(projectId: String): Params {
                return Params(
                    projectId
                )
            }
        }

    }

    override fun buildCaseCompeletable(params: Params?): Completable {
        if(params == null) throw IllegalArgumentException("Params can't be empty")
        return projectRepository.unbookmarkProject(params.projectId)
    }


}